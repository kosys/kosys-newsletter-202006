"use strict";

var fs = require('fs');
var path = require('path');
var gulp = require("gulp");
var webserver = require("gulp-webserver");

gulp.task("print", async function(done) {
    resetVuePressState();
    var vuepress = require("vuepress");
    return vuepress.build({sourceDir: ".", theme: '@kosys-docs/vuepress-theme-print', print: true});
});

gulp.task("web", function(done){
    resetVuePressState();
    var vuepress = require("vuepress");
    return vuepress.build({sourceDir: ".", theme: '@vuepress/theme-default', print: false});
});

gulp.task("webserver", function(done){
    return gulp.src('public')
            .pipe(webserver({
                host: 'localhost',
                port: 4000,
                path: "/kosys-newsletter-202006/",
                baseDir: "public",
                livereload: true,
                middleware: function (req, res, next) {
                  res.setHeader('Access-Control-Allow-Origin', '*');
                  next();
                },
            }));
});


gulp.task("default", gulp.series("web", "print"));

gulp.task("watch", function(){
    return gulp.watch(["**/*", "./.vuepress/**/*", "!public/**/*"]).on('change', gulp.series("default"));
})

gulp.task("serve", gulp.series("default", gulp.parallel("webserver", "watch")));


function resetVuePressState() {

    // vuepressのAppContextはシングルトンであり、引数違いの連続実行がそのままでは行えない仕様のため
    // 状態をクリアする必要がある

    for (var key in require.cache) {
        if(key.indexOf("@vuepress") >= 0) {
            delete require.cache[ key ];
        }
    }
}