
export default {
    created () {
      if (this.$ssrContext) {
        // style tag to override css
        // for styles which cannot be scoped such as @page rule.
        
        this.$ssrContext.overrideStyle = '';

        const userOverrideStyles = this.$frontmatter.overrideStyles;

        var style = renderOverrideStyle(userOverrideStyles); 

        if (style !== ""){
          this.$ssrContext.overrideStyle = `<style>${style}</style>`;
        }

        function renderOverrideStyle(data) {
          
          var style = ""; 
          
          if (Array.isArray(data)){
            style = data.join("\n");
          } else if (typeof (data) == "string") {
            style = data;
          }

          return " " + style + " ";
        }
      }
    }
}