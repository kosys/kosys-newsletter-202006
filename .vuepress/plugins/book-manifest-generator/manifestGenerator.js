"use strict";

const { fs, path, parseFrontmatter } = require('@vuepress/shared-utils');

module.exports = (ctx) => {

    function convertSidebarToBookManifest(sidebar) {
        return {
            "@context": ["https://schema.org", "https://www.w3.org/ns/wp-context"],
            "type": "Book",
            "url": ctx.siteConfig.bookConfig.bookUrl,
            "author": ctx.siteConfig.bookConfig.author,
            "dateModified": "",
            "readingOrder": getAllPageUrls(sidebar),
        };
    }

    function getAllPageUrls(nodes) {
        
        var pageUrls  = [];
            
        nodes.forEach(function(i) {

            var node_type = "";

            if (typeof(i) === "string") {
                node_type = "section";
            } else {
                node_type = "chapter";
            }

            if (node_type == "chapter") {

                if(i.path) {
                    const page = findPage(i.path);
                    pageUrls.push(getUrl(page.regularPath));

                }
                if(i.children) {
                    pageUrls = pageUrls.concat(getAllPageUrls(i.children));
                }

            } else if(node_type == "section") {
                const page = findPage(i);

                if(page) {
                    pageUrls.push(getUrl(page.regularPath));
                }
            }
        });

        return pageUrls;
    }

    function findPage(path) {
        const foundPages = ctx.pages.filter(page => path === page.regularPath || path + ".html" === page.regularPath  );
        if (foundPages.length > 0) {
            return foundPages[0];
        } else {
            return null;
        }
    }

    function getUrl(pagePath) {

        if (pagePath.startsWith("/")) {
            pagePath = pagePath.slice(1);
        }
        if (pagePath == "") {
            pagePath = "index.html";
        }
        var url = ctx.siteConfig.base + pagePath;
        return url;
    }

    return {
        generateBookManifest() {
            const sidebar = ctx.themeConfig.sidebar;
            const manifest = convertSidebarToBookManifest(sidebar);

            const manifestFilePath =  path.join(ctx.outDir,  "manifest.json");
            fs.writeFileSync(manifestFilePath, JSON.stringify(manifest));
        }
    }
}
