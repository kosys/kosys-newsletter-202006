
const { path } = require('@vuepress/shared-utils');

module.exports = (ctx) => {

    var plugins = [
    //    '@kosys-docs/vuepress-plugin-search'
    ];
    var base = "/kosys-newsletter-202006/";
    var dest = "public/";

    //印刷モードの場合
    if (ctx.options.theme === "@kosys-docs/vuepress-theme-print") {
        theme = '@kosys-docs/vuepress-theme-print';
        plugins.push("@kosys-docs/book-manifest-generator");
        base += "print/";
        dest = "public/print/";
    } else {
        theme = "@kosys-docs/vuepress-theme-local";
    }

    return {
        title: 'こうしす！ Newsletter 2020.6',
        description: '会報',
        base: base,
        dest: dest,
        locales: {
            "/": {
            lang: "ja"
            }
        },
        plugins: plugins,
        theme: theme,
        themeConfig: {
            sidebar: [
                '/frontcover',
                '/',
                '/message',
                {
                    title: '活動報告',
                    path: "/chapter1_index",
                    collapsable: false,
                    children: [
                        '/chapter1_section1',
                        '/chapter1_section2'
                    ]
                },
                {
                    title: '寄稿記事',
                    path: "/chapter2_index",
                    collapsable: false,
                    children: [
                        '/chapter2_773',
                        '/chapter2_ibuta',
                        '/chapter2_tamamusi',
                    ]
                },
                '/postscript',
                '/colophon',
                '/backcover',
            ]
        },
        bookConfig: {
            author: "OPAP-JP contributors",
            bookUrl: "https://kosys.gitlab.io/kosys-newsletter-202006/print/",
            cover: "cover.png",
            coverFormat: "image/png"
        },
        postcss: {
            plugins: []
        },
        chainWebpack: config => {
            // 最適化処理はVivliostyleと相性が悪いので削除する
            config.plugins.delete('optimize-css')
        },
    };
};