---
title: 表表紙
author: 
sectionClass: frontcover
overrideStyles: "
@page {
}
@page:left {
    @top-left {
        content: '';
        border-bottom: none;
    }
}

@page:right {
    @top-right {
        content: '';
        border-bottom: none;
    }
}"
---
<img src="./images/cover.png" class="img-fullpage"  />
