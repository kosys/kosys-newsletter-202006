---
layout: ChapterCoverLayout
overrideStyles: "
@page {
}
@page:left {
    @top-left {
        content: '';
        border-bottom: none;
    }
}

@page:right {
    @top-right {
        content: '';
        border-bottom: none;
    }
}"
---
# 活動報告

OPAP-JPの2019-2020年を振り返ります
