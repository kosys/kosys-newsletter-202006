---
sectionClass: colophon-section
overrideStyles: "
@page {
}
@page:left {
    @top-left {
        content: '';
        border-bottom: none;
    }
}

@page:right {
    @top-right {
        content: '';
        border-bottom: none;
    }
}"
---
# 奥付

<h2 class="book-title">こうしす！NEWSLETTER 2020年号</h2>
<table class="print-history">
    <tr>
        <td class="date">2020年12月21日</td>
        <td class="description">初版第1刷　発行</td>
    </tr>
</table>
<table class="book-credit">
    <tr>
        <th>発行者</th>
        <td>
            オープンプロセスアニメプロジェクトジャパン<br />
            <a href="https://opap.jp/">https://opap.jp/</a>
        </td>
    </tr>
    <tr>
        <th>表紙デザイン</th>
        <td>井二かける</td>
    </tr>
    <tr>
        <th>表紙イラスト</th>
        <td>草宮るみあ、廣田智亮</td>
    </tr>
    <tr>
        <th>イラスト寄稿</th>
        <td>
            那々海ゆあ
        </td>
    </tr>
    <tr>
        <th>記事寄稿</th>
        <td>
            玉虫型偵察器、森野 憂希
        </td>
    </tr>
    <tr>
        <th>編集</th>
        <td>森野 憂希、井二かける</td>
    </tr>
    <tr>
        <th>素材提供</th>
        <td>
            &copy;OPAP-JP contributors (<a href="https://opap.jp/contributors">https://opap.jp/contributors</a>) <br />
            クリエイティブ・コモンズ 表示 4.0 国際ライセンスの許諾に基づき、一部を改変して掲載
        </td>
    </tr>
    <tr>
        <th>著作権表記</th>
        <td>
            &copy;OPAP-JP contributors (<a href="https://opap.jp/contributors">https://opap.jp/contributors</a>) <br />
            特に記載のない限り、クリエイティブ・コモンズ 表示 4.0 国際ライセンス（<a href="https://creativecommons.org/licenses/by/4.0/deed.ja">https://creativecommons.org/licenses/by/4.0/deed.ja</a>）のもとに利用を許諾。
        </td>
    </tr>
    <tr>
        <th>印刷所</th>
        <td>
            ちょ古っ都製本工房
            <aside style="font-size: 0.8em; background-color: #EEE; padding: 1em; border-radius: 2mm; margin: 2mm 0;">
            ※入稿設定: <br />
            冊子のサイズ: A5 / 本文の印刷方法：カラー・モノクロ混在印刷　お得ver. / ページ数： 44ページ（内カラーページ数：16ページ 巻頭） / 表紙の種類：アートポスト紙・２００ｋｇ（高品質フルカラー印刷）/ 表紙の色：白（200kg）【アートポスト紙】 / 本文の種類：上質９０ｋｇ / オプション加工：表紙・PP加工（クリアPP）/ 製本方法：くるみ製本 / とじ方：左とじ
            </aside>
        </td>
    </tr>
</table>

<div class="small">
Printed in Japan.<br />
<br />
乱丁、落丁はご容赦ください。<br />
<br />
この印刷物は非売品です。当団体の会報誌として、会員及び当団体に特別の関係を有する者に対して無償頒布するために印刷されたものです。この印刷物そのものを有償で譲渡、販売、転売等をすることはご遠慮ください。なお、この規定は本冊子の内容の利用を制限するものではありません。
</div>
