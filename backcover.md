---
title: 裏表紙
author: 
sectionClass: backcover
overrideStyles: "
@page {
}
@page:left {
    @top-left {
        content: '';
        border-bottom: none;
    }
}

@page:right {
    @top-right {
        content: '';
        border-bottom: none;
    }
}"
---

<img src="./images/cover-back.png" class="img-fullpage"  />