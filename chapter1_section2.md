---
balanceSheetData: [
  {"accountName": "Ⅰ 資産の部", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-0 item-group-header"},
  {"accountName": "1.流動資産", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-1 item-group-header"},
  {"accountName": "現金預金", "balance1": 16261, "balance2": null, "balance3": null, "cssClass": "item-depth-2"},
  {"accountName": "流動資産合計", "balance1": null, "balance2": 16261, "balance3": null, "cssClass": "item-depth-2 item-sum item-break-1"},
  {"accountName": "2.固定資産", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-1 item-group-header"},
  {"accountName": "固定資産合計", "balance1": null, "balance2": 0, "balance3": null, "cssClass": "item-depth-2 item-sum item-break-1"},
  {"accountName": "資産合計", "balance1": null, "balance2": null, "balance3": 16261, "cssClass": "item-depth-1 item-sum item-break-2"},
  {"accountName": "Ⅱ 負債の部", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-0 item-group-header item-break-3"},
  {"accountName": "1.流動負債", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-1"},
  {"accountName": "役員借入金", "balance1": 65033, "balance2": null, "balance3": null, "cssClass": "item-depth-2"},
  {"accountName": "流動負債合計", "balance1": null, "balance2": 65033, "balance3": null, "cssClass": "item-depth-2 item-sum item-group-header item-break-1"},
  {"accountName": "2.固定負債", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-1"},
  {"accountName": "固定負債合計", "balance1": null, "balance2": 0, "balance3": null, "cssClass": "item-depth-2 item-sum item-break-1"},
  {"accountName": "負債合計", "balance1": null, "balance2": null, "balance3": 65033, "cssClass": "item-depth-1 item-sum item-break-2"},
  {"accountName": "Ⅲ 正味財産の部", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-0 item-group-header"},
  {"accountName": "前期繰越正味財産", "balance1": null, "balance2": -51893, "balance3": null, "cssClass": "item-depth-2"},
  {"accountName": "当期正味財産増減額", "balance1": null, "balance2": 3121, "balance3": null, "cssClass": "item-depth-2"},
  {"accountName": "正味財産合計", "balance1": null, "balance2": null, "balance3": -48772, "cssClass": "item-depth-1 item-sum item-break-2"},
  {"accountName": "負債及び正味財産合計", "balance1": null, "balance2": null, "balance3": 16261, "cssClass": "item-depth-1 item-sum item-break-3"},
]
profitAndLossData: [
  {"accountName": "I 経常収益", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-0 item-group-header"},
  {"accountName": "1.受取会費", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-1 item-group-header"},
  {"accountName": "正会員受取会費", "balance1": 5000, "balance2": null, "balance3": null, "cssClass": "item-depth-2"},
  {"accountName": "賛助会員受取会費", "balance1": 62467, "balance2": 67467, "balance3": null, "cssClass": "item-depth-2"},
  {"accountName": "2.受取寄附金", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-1 item-group-header item-break-1"},
  {"accountName": "受取寄附金", "balance1": 624960, "balance2": 624960, "balance3": null, "cssClass": "item-depth-2"},
  {"accountName": "3.その他収益", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-1 item-group-header item-break-1"},
  {"accountName": "雑収益", "balance1": 42809, "balance2": 42809, "balance3": null, "cssClass": "item-depth-2"},
  {"accountName": "経常収益計", "balance1": null, "balance2": null, "balance3": 735236, "cssClass": "item-depth-1 item-sum item-break-1 item-break-2"},
  {"accountName": "II 経常費用", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-0 item-group-header"},
  {"accountName": "1.事業費", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-1 item-group-header"},
  {"accountName": "（1）人件費", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-2 item-group-header"},
  {"accountName": "人件費計", "balance1": 0, "balance2": null, "balance3": null, "cssClass": "item-depth-3 item-sum item-break-1"},
  {"accountName": "（2）その他経費", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-2 item-group-header item-break-1"},
  {"accountName": "業務委託費", "balance1": 445263, "balance2": null, "balance3": null, "cssClass": "item-depth-3"},
  {"accountName": "通信運搬費", "balance1": 21606, "balance2": null, "balance3": null, "cssClass": "item-depth-3"},
  {"accountName": "消耗品費", "balance1": 4683, "balance2": null, "balance3": null, "cssClass": "item-depth-3"},
  {"accountName": "支払手数料", "balance1": 57520, "balance2": null, "balance3": null, "cssClass": "item-depth-3"},
  {"accountName": "広告宣伝費", "balance1": 9441, "balance2": null, "balance3": null, "cssClass": "item-depth-3"},
  {"accountName": "その他経費計", "balance1": 538513, "balance2": null, "balance3": null, "cssClass": "item-depth-3 item-sum item-break-1"},
  {"accountName": "事業費計", "balance1": null, "balance2": 538513, "balance3": null, "cssClass": "item-depth-2 item-sum item-break-1"},
  {"accountName": "2.管理費", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-1 item-group-header"},
  {"accountName": "（1）人件費", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-2 item-group-header"},
  {"accountName": "人件費計", "balance1": 0, "balance2": null, "balance3": null, "cssClass": "item-depth-3 item-sum item-break-1"},
  {"accountName": "（2）その他経費", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-2 item-group-header item-break-1"},
  {"accountName": "印刷製本費", "balance1": 37072, "balance2": null, "balance3": null, "cssClass": "item-depth-3"},
  {"accountName": "通信運搬費", "balance1": 124418, "balance2": null, "balance3": null, "cssClass": "item-depth-3"},
  {"accountName": "消耗品費", "balance1": 1183, "balance2": null, "balance3": null, "cssClass": "item-depth-3"},
  {"accountName": "支払手数料", "balance1": 41203, "balance2": null, "balance3": null, "cssClass": "item-depth-3"},
  {"accountName": "その他経費計", "balance1": 203876, "balance2": null, "balance3": null, "cssClass": "item-depth-3 item-sum item-break-1"},
  {"accountName": "管理費計", "balance1": null, "balance2": 203876, "balance3": null, "cssClass": "item-depth-2 item-sum item-break-1"},
  {"accountName": "経常費用計", "balance1": null, "balance2": null, "balance3": 742389, "cssClass": "item-depth-1 item-sum item-break-2"},
  {"accountName": "当期経常増減額", "balance1": null, "balance2": null, "balance3": -7153, "cssClass": "item-depth-2 item-sum item-group-header item-break-3"},
  {"accountName": "III経常外収益", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-0 item-group-header"},
  {"accountName": "過年度損益修正益", "balance1": 10274, "balance2": null, "balance3": null, "cssClass": "item-depth-2 item-group-header"},
  {"accountName": "経常外収益計", "balance1": 10274, "balance2": null, "balance3": null, "cssClass": "item-depth-2 item-sum item-break-1"},
  {"accountName": " 当期正味財産増減額", "balance1": null, "balance2": null, "balance3": 3121, "cssClass": "item-depth-0 item-break-1 item-break-3"},
  {"accountName": "前期繰越正味財産額", "balance1": null, "balance2": null, "balance3": -51893, "cssClass": "item-depth-2 item-sum"},
  {"accountName": "次期繰越正味財産額", "balance1": null, "balance2": null, "balance3": -48772, "cssClass": "item-depth-2 item-sum item-break-3"},
]
---
# 2019年度決算報告

## 期間
2019年1月1日から2019年12月31日まで

## 活動計算書
<div class="unit">
    単位：円
</div>
<FinancialStatementsTable :sheetData="$page.frontmatter.profitAndLossData" />

## 貸借対照表
<div class="unit">
    単位：円
</div>
<FinancialStatementsTable :sheetData="$page.frontmatter.balanceSheetData" />


## 財務諸表の注記

### 重要な会計方針

財務諸表の作成は、NPO法人会計基準（2010年7月20日　2017年12月12日最終改正　NPO法人会計基準 協議会）によっています。ただし、財産目録の作成は省略しております。

### 施設の提供等の物的サービスの受入の内訳

当期は計上しておりません。

### 活動の原価の算定にあたって必要なボランティアによる役務の提供の内訳

当期は計上しておりません。

### 借入金の増減内訳
<div class="unit">
    単位：円
</div>
<table class="financialSheet">
    <tr>
        <th>科目</th>
        <th>当期残高</th>
        <th>当期借入</th>
        <th>当期返済</th>
        <th>期末残高</th>
    </tr>
    <tr>
        <td>役員借入金</td>
        <td>54,000</td>
        <td>76,753</td>
        <td>65,720</td>
        <td>65,033</td>
    </tr>
    <tr>
        <td>計</td>
        <td>54,000</td>
        <td>76,753</td>
        <td>65,720</td>
        <td>65,033</td>
    </tr>
</table>


### 役員及びその近親者との取引の内容

役員及びその近親者との取引は以下の通りです。

#### 活動計算書

<div class="unit">
    単位：円
</div>
<table class="financialSheet">
    <tr>
        <th>科目</th>
        <th>計算書類に計上された金額</th>
        <th>内、役員との取引</th>
        <th>内、近親者及び支配法人等との取引</th>
    </tr>
    <tr>
        <td>正会員受取会費</td>
        <td>5,000</td>
        <td>5,000</td>
        <td>0</td>
    </tr>
    <tr>
        <td>雑収益</td>
        <td>39,954</td>
        <td>39,954</td>
        <td>0</td>
    </tr>
    <tr>
        <td>計</td>
        <td>44,954</td>
        <td>44,954</td>
        <td>0</td>
    </tr>
</table>

#### 貸借対照表

<div class="unit">
    単位：円
</div>
<table class="financialSheet">
    <tr>
        <th>科目</th>
        <th>計算書類に計上された金額</th>
        <th>内、役員との取引</th>
        <th>内、近親者及び支配法人等との取引</th>
    </tr>
    <tr>
        <td>役員借入金</td>
        <td>65,033</td>
        <td>65,033</td>
        <td>0</td>
    </tr>
    <tr>
        <td>計</td>
        <td>65,033</td>
        <td>65,033</td>
        <td>0</td>
    </tr>
</table>


<style lang="stylus">
.unit {
  text-align: right ;
}

.financialSheet{
  width: 100%;
  table-layout: fixed;
  border: solid 1px black;
  display: table;
  margin: 0;

  * {
    background: none;
  }

  tr {
    border: none;
  }
  th {
    border: solid 1px black;
  }
  
  td {
    border: solid 1px black;
    border-top: none;
    border-bottom: none;
    padding: 5px;
  }

  .item-depth-0 {
    .accountName {  }
  }
  .item-depth-1 {
    .accountName { padding-left: 2em; }
  }
  .item-depth-2 {
    .accountName { padding-left: 4em; }
  }
  .item-depth-3 {
    .accountName { padding-left: 6em; }
  }

  .balance {
    text-align: right;
  }

  .item-sum {
    .balance, .accountName { font-weight: bold; }
  }

  .item-group-header .accountName { font-weight: bold; }
  
  .item-break-1 .balance-1 { border-top: solid 1px black; }
  .item-break-2 .balance-2 { border-top: solid 1px black; }
  .item-break-3 .balance-3 { border-top: solid 1px black; }
  
}

.col-accountName { width: auto; }
.col-balance { width: 8em; }


.print .financialSheet  { 
    font-size: 8pt;
        
    .col-accountName { width: auto; }
    .col-balance { width: 6em; }
}
</style>

