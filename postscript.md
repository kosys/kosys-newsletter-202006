# 編集後記

本当は6月刊行予定でしたが、今年は予定外のことがたくさん起こり、ついに年末となってしまいました。
次回はもっと早く刊行できるよう頑張ります。

<AuthorProfiles>
<AuthorProfileItem authorName="井二かける">
</AuthorProfileItem>
</AuthorProfiles>

<div style="clear: both;"></div>
<div class="donate">
<strong>― 寄附歓迎！―</strong> <br />
当団体へのご支援は主に以下のウェブサイトにて受け付けております。 <br />
(※2020年12月現在の情報です)
<table>
    <tr>
        <td>
        <img src="./images/qrcode_syncable.png" />
Syncable - ご寄附<br />
<a href="https://syncable.biz/associate/opap-jp/donate/">https://syncable.biz/associate/opap-jp/donate/</a>
        </td>
        <td>
            <img src="./images/2dcode_pixiv.png" />
    PIXIV FANBOX - 賛助会員<br />
    <a href="https://www.pixiv.net/fanbox/creator/6788743">https://www.pixiv.net/fanbox/creator/6788743</a>
        </td>
    </tr>
</table>
</div>